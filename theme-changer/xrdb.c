//https://gist.github.com/dcat/05dc5d45e8c60f2645da
#include <stdio.h>
#include <X11/Xlib.h>
#include <string.h>
#include <X11/Xresource.h>

Bool get_xresource(Display *dpy, const char* name, char* dst)
{
  char *type;
  XrmValue ret;
  XrmDatabase db;
  char *resource_manager;

  XrmInitialize();
  resource_manager = XResourceManagerString(dpy);

  if (resource_manager == NULL)
    return False;

  db = XrmGetStringDatabase(resource_manager);

  if (db == NULL)
    return False;

  XrmGetResource(db, name, "String", &type, &ret);

  if (ret.addr != NULL && !strncmp("String", type, 64)) {
    strcpy(dst, ret.addr);
    return True;
  }
  return False;
}

int main(int argc, char *argv[])
{
  Display* display = XOpenDisplay (0);
  char resource[256];
  if(argc == 2) {
    if(get_xresource(display, argv[1], resource)) {
      printf(resource);
      return 0;
    }
  }
  return 1;
}
