# this templater is inspired by https://github.com/moustacheful/dotfiles
import nre, strutils, future, os, ospaths, Terminal
  , commandeer
from osproc import execCmdEx
#os.setCurrentDir(os.getAppDir())

commandline:
  argument filepath, string
  option toStdout, bool, "to-stdout", "O"
  option verbose, bool, "verbose", "v"
  exitoption "help", "h",
             "Usage: templater [options] <file>\n\n" &
             "Options:\n" &
             "	-O, --to-stdout		Output to stdout instead of a file\n" &
             "	-v, --verbose		Enable verbose output\n"

proc parseTemplate*(file: string, parserFn: proc) =
  if fileExists(file & ".template"):
    # Regex pattern: https://stackoverflow.com/questions/7124778/how-to-match-anything-up-until-this-sequence-of-characters-in-a-regular-expres
    let newContent = readFile(file & ".template").replace(
      re"(?i){{(.+?)}}", 
      (match: RegexMatch) => parserFn(match.captures[0].strip())
    )
    if toStdout:
      stdout.write newContent
    else:
      file.writeFile(newContent)
  else:
    echo("File " & file & ".template not found")
    quit(1)

proc getBashOutput(str: string): string =
  var (output, err) = execCmdEx(str)
  if (err>0):
    setForegroundColor(fgRed)
    echo "Failed parsing command: \"" & str & "\""
    echo "Output is: \"" & output & "\""
    quit(1)
  # Remove any newlines at the end of output
  output.removeSuffix('\n')
  if verbose and not toStdout:
    stdout.write "\"" & str & "\" " 
    setForegroundColor(fgGreen)
    stdout.write "->"
    resetAttributes()
    echo " \"" & output & "\""
  return output#.replace("\n")


when isMainModule:
  var absPath: string
  if filepath.isAbsolute:
    absPath = filepath
  else:
    absPath = joinPath(getCurrentDir(), filepath)
  let pDir = parentDir(absPath)
  let file = extractFileName(filepath)
  if verbose and not toStdout:
    echo("Parent dir:" & pDir)
  # Set the directory for file operations and bash commands 
  # to the directory containing the file specified
  os.setCurrentDir(pDir)

  parseTemplate(file, getBashOutput)
