function server
  firefox "http://localhost:$argv[1]/" &
  # Set the default Content-Type to `text/plain` instead of `application/octet-stream`
  # And serve everything as UTF-8 (although not technically correct, this doesn’t break anything for binary files)
  python -c 'import SimpleHTTPServer
map = SimpleHTTPServer.SimpleHTTPRequestHandler.extensions_map
map[""] = "text/plain"
for key, value in map.items():
  map[key] = value + ";charset=UTF-8"
SimpleHTTPServer.test()' $argv[1]
end

function mcd
  mkdir $argv[1]; and cd $argv[1]
end

function datem
  set DATE '+%d/%m/%Y - %A'
  echo (date "$DATE") | xclip -selection c
end

#http://lewandowski.io/2016/10/fish-env/
function posix-source
  for i in (cat $argv)
    set arr (string split -m1 = $i)
      set -gx $arr[1] $arr[2]
  end
end

# Enable fzf key bindings
if status --is-interactive
  which fzf > /dev/null && fzf_key_bindings
end

function git_down
  git checkout HEAD^
end

function git_up 
  git log --reverse --pretty=%H $argv | grep -A 1 (git rev-parse HEAD) | tail -n1 | xargs git checkout
end
