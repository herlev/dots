set PATH ~/.bin $PATH
set PATH $PATH ~/.local/bin
set PATH $PATH ~/.npm/bin
set PATH $PATH ~/.linuxbrew/bin
set PATH $PATH ~/.local/share/cargo/bin
set PATH $PATH ~/.apps-bin
set PATH $PATH ~/.grab/bin
set PATH $PATH ~/go/bin
set PATH $PATH ~/.local/share/go/bin
set PATH $PATH ~/anaconda3/bin
set PATH $PATH ~/.gem/ruby/2.7.0/bin
set PATH $PATH ~/.luarocks/bin
set PATH $PATH ~/miniconda3/bin/
set PATH $PATH ~/.local/share/gem/ruby/3.0.0/bin
set PATH $PATH ~/.cargo/bin
set PATH $PATH ~/.local/share/swim/bin/oss-cad-suite/bin/gowin_pack
set PATH $PATH ~/.nimble/bin
