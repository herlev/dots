# Easier navigation: .., ..., ...., ....., ~ and -
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias .-="cd -"

# Get week number
alias week='date +%V'

# Create links
alias link="ln -sf"

# Ip
alias publicip="curl ifconfig.co"
alias localip="/bin/ip address show wlan0 | grep 'inet ' | awk '{print \$2}' | cut -d'/' -f1"
alias ips="/bin/ip -o addr | awk '!/^[0-9]*: ?lo|link\/ether/ {print \$2\" \"\$4}'"
alias ip="ip --color=auto"

# List files in long format
alias ll="ls -l"

# List all files
alias la="ls -a"

# List only directories
alias lsd="ls -D"

# Use exa for ls
if command -q exa
    alias ls="exa -F"
end
# Use bat for cat
if command -q bat
    alias cat="bat --paging=never --style=numbers,grid"
end

# List all files with git status
alias lsg="ls -la --git"

# Lazygit
alias lg="lazygit"

# cd to temp directory
alias tcd="cd (mktemp -d)"

# Test colorscheme
alias colortest="msgcat --color=test"

alias rm="rm -vI"

alias octave="octave --quiet"

alias open="xdg-open"

alias bopen="b xdg-open"

alias ubuntu="distrobox enter ubuntu-22-04"

alias hn="clx -n"
alias pp="pipenv run python"

alias nb=numbat
