set fish_greeting
set fish_path ~/.config/fish
# set -gx EDITOR (which hx)

# https://stackoverflow.com/questions/30662735/how-to-map-ctrl-z-to-fg-in-fish
bind \cq 'if test -z (commandline) ; fg; else ; clear; commandline ""; end'

# Enable colored man pages https://www.2daygeek.com/get-display-view-colored-colorized-man-pages-linux/
set -xU MANROFFOPT "-P -c"
set -xU LESS_TERMCAP_md (printf "\e[01;31m")
set -xU LESS_TERMCAP_me (printf "\e[0m")
set -xU LESS_TERMCAP_se (printf "\e[0m")
set -xU LESS_TERMCAP_so (printf "\e[01;44;33m")
set -xU LESS_TERMCAP_ue (printf "\e[0m")
set -xU LESS_TERMCAP_us (printf "\e[01;32m")

. $fish_path/aliases.fish
. $fish_path/pacman-aliases.fish
. $fish_path/functions.fish
# . $fish_path/paths.fish
. $fish_path/functions/yazi-quitcd.fish

if test -f /opt/ros/humble/setup.bash; bass source /opt/ros/humble/setup.bash; end

if type -q antidot; antidot init | source; end
if type -q zoxide; zoxide init fish | source; end

# distrobox
function fish_command_not_found
  # "In a container" check
  if test -e /run/.containerenv -o -e /.dockerenv
    distrobox-host-exec $argv
  else
    __fish_default_command_not_found_handler $argv
  end
end