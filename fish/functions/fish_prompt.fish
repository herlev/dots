
set __fish_git_prompt_showdirtystate 'yes'
set __fish_git_prompt_showstashstate 'yes'
set __fish_git_prompt_showuntrackedfiles 'yes'
set __fish_git_prompt_showupstream 'yes'
set __fish_git_prompt_color_branch yellow
set __fish_git_prompt_color_upstream_ahead green
set __fish_git_prompt_color_upstream_behind red

function p
    echo (string replace $HOME '~' $PWD)
end

function uh
    set_color $color_primary
    echo -n (whoami)
    set_color normal
    echo -n " @ "
    set_color $color_primary
    echo (prompt_hostname)
    set_color normal
end

# Credit to this guy: https://adrian-philipp.com/post/cmd-duration-fish-shell
function printtime --on-event fish_postexec
    # Handle long execution times
    if test $CMD_DURATION
        set -l duration (echo "$CMD_DURATION 1000" | awk '{printf "%.3fs", $1 / $2}')
        set -l exclude "bash|fish|open|more|less|vim|nano|nvim|vi|man|ssh|n|l|d"
        if test $CMD_DURATION -gt 5000
            echo "  ( "$duration" )"
        end
        if test $CMD_DURATION -gt 10000
          if test (echo "$argv" | grep -vE "^($exclude).*\$")
            which notify-send > /dev/null && notify-send -t 5000 -u normal "$argv" "Returned $status, in $duration"
          end
        end
    end
end

function cleanup_history
    set last $history[1]
    if echo "$last" | grep -qe "rm -r" -qe "^pass " -qe "^pass\$" -qe "^trans "
      history delete --exact --case-sensitive -- "$last"
    end
end

# https://github.com/neovim/neovim/issues/4867
function reset-cursor
  printf "\e[6 q"
end

function fish_prompt --description 'Write out the prompt'
    set -l last_status $status
    cleanup_history
    reset-cursor
    
    if test (hostname) != "YS7"
        set_color yellow
        printf [(hostname -s)]" " 
        set_color normal
    end

    set_color $color_primary
    #echo -n (prompt_pwd)
    echo -n (basename (p))
    set_color normal
    if not test $last_status -eq 0
        set_color $fish_color_error
    end
    echo -n ' ➤ '
    # ❯
    set_color normal
end

function fish_right_prompt
    echo (__fish_git_prompt)
end
