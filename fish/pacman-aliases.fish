# Credit:
# https://rhnotebook.wordpress.com/2010/01/31/handy-pacman-aliases/
# search for a package
alias pms="paru -Ss --bottomup" 
 # see info on a package
alias pmq="pacman -Q"
# see detailed info on a package
alias pmqi="pacman -Qi" 
# see files installed by a package
alias pmql="pacman -Ql" 
# install a package
alias pmi="paru -Sy --noconfirm" 
# remove a package, including unneeded deps
alias pmr="sudo pacman -Rns" 
# check for updates
alias pmc="paru -Sy; and paru -Qu" 
# update system
alias pmu="sudo pacman -Sy; and sudo pacman -Su" 

# get help
alias pmh="cat "(status --current-filename)
