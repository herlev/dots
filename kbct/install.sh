#!/usr/bin/env bash

set -Eeuo pipefail

sudo cp kbct.service /etc/systemd/system/
sudo cp keymap.yml /usr/local/share/kbct-remap.yml

sudo systemctl daemon-reload

sudo systemctl enable --now kbct