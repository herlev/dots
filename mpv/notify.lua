-- from: https://github.com/emilazy/mpv-notify-send/blob/master/notify-send.lua
local utils = require "mp.utils"

function notify(summary, body)
    return mp.command_native({
        "run", "notify-send", summary, body,
    })
end

function escape_pango_markup(str)
    return string.gsub(str, "([\"'<>&])", function (char)
        return string.format("&#%d;", string.byte(char))
    end)
end

function notify_current_media()
    local thumbnail = nil
    local title = mp.get_property_native("media-title")

    return notify("Now playing", escape_pango_markup(title))
end

mp.register_event("file-loaded", notify_current_media)
