exec-once=dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
exec-once=/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
exec-once=systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
exec-once=mkdir /tmp/kittysocks
# exec-once=kbct remap --config ~/.config/kbct/keymap.yml
exec-once=waybar > /tmp/waybar.log
exec-once=swww-daemon
exec-once=dunst
# exec-once=swayidle before-sleep 'waylock -init-color "0x111111" -input-color "0x333333"'
exec-once=hyprpm enable hyprexpo
exec-once=avizo-service
exec-once=nm-applet --indicator
exec-once=hyprctl setcursor phinger-cursors-dark 32
exec-once=playerctld daemon
exec-once=/usr/lib/kdeconnectd
exec-once=kdeconnect-indicator
exec-once=syncthing-gtk -m

# Autostart applications on specific workspaces
exec-once=[workspace 1 silent] firefox
exec-once=[workspace 2 silent] kitty --listen-on "unix:/tmp/kittysocks/$(uuidgen)" tmux
exec-once=[workspace 7 silent] thunderbird


monitor=eDP-1,preferred,0x360,1
monitor=,preferred,auto,1
workspace=DP-1,1


# https://wiki.archlinux.org/title/Xorg/Keyboard_configuration#Setting_keyboard_layout
# https://www.codejam.info/2022/04/xmodmaprc-wayland.html
# caps-ctrl-ijkl doesn't work
input {
    kb_file=
    kb_layout=dk
    kb_variant=nodeadkeys
    kb_model=
    #kb_options=ctrl:swap_lalt_lctl
    kb_options=
    kb_rules=
    repeat_rate=50
    repeat_delay=200

    follow_mouse=1
	#float_switch_override_focus=0


    touchpad {
        natural_scroll=yes
        disable_while_typing=yes
    }

    sensitivity=0 # -1.0 - 1.0, 0 means no modification.
}

#device:AT Translated Set 2 keyboard {
#    kb_options=ctrl:swap_lalt_lctl
#}
#
#device:input-remapper AT Translated Set 2 keyboard forwarded {
#    kb_options=ctrl:swap_lalt_lctl
#}


general {
    gaps_in=5
    gaps_out=20
    border_size=4
    col.active_border = rgb(5ce4ff) rgb(0067a8) 45deg
    col.inactive_border=0x66333333

    #apply_sens_to_raw=0 # whether to apply the sensitivity to raw input (e.g. used by games where you aim using your mouse)
    #no_cursor_warps=true

    layout=master
}

debug {
    disable_logs = false
}

cursor {
    hide_on_key_press = true
}

misc {
    disable_hyprland_logo=true
    middle_click_paste=false
    # enable_swallow=true
    # swallow_regex=kitty
}

decoration {
    rounding=10
    blur {
        size=5 # minimum 1
        passes=1 # minimum 1
        new_optimizations=1
    }
}

group {
    col.border_active = rgb(5ce4ff) rgb(0067a8) 45deg
    col.border_inactive = rgba(00000000)
    col.border_locked_active = rgb(5ce4ff) rgb(0067a8) 45deg
    col.border_locked_inactive = rgba(00000000)
    groupbar {
        font_family = "Jetbrains Mono Regular"
        font_size = 10
        height = 16
        text_color = rgb(ffffff)
        #col.active = rgb(5ce4ff)
        col.active = rgb(35596a)   
        col.inactive = rgba(25313fe7)
        col.locked_active = rgb(35596a)   
        col.locked_inactive = rgba(25313fe7)
    }
}

animations {
    enabled=1
    bezier=overshot,0.05,0.9,0.1,1.05
    animation = windows, 1, 5, overshot, slide
    animation = border, 1, 10, default
    animation = fade, 1, 5, default
    animation = workspaces, 1, 3, default
}

dwindle {
    pseudotile=0 # enable pseudotiling on dwindle
    #new_on_top=true
    # no_gaps_when_only = true
}

gestures {
    workspace_swipe=yes
    workspace_swipe_distance=200
    #workspace_swipe_min_speed_to_force=1
    workspace_swipe_cancel_ratio=0.2
    workspace_swipe_create_new=0
    workspace_swipe_forever=1
}

master {
    # no_gaps_when_only=0
    #new_is_master=false
}

binds {
    workspace_back_and_forth=true
    allow_workspace_cycles=true # until https://github.com/hyprwm/Hyprland/issues/2691 is fixed
    movefocus_cycles_fullscreen = false # doesn't seem to be working
}

# example window rules
# for windows named/classed as abc and xyz
#windowrule=move 69 420,abc
#windowrule=size 420 69,abc
#windowrule=tile,xyz
#windowrule=float,abc
#windowrule=pseudo,abc
#windowrule=monitor 0,xyz


# not working?
windowrule=tile,class:spotify

# some nice mouse binds
bindm=SUPER,mouse:272,movewindow
bindm=SUPER,mouse:273,resizewindow

# example binds
bind=SUPER,RETURN,exec,kitty --listen-on "unix:/tmp/kittysocks/$(uuidgen)"
bind=SUPER_SHIFT,U,exec,kitty distrobox enter ubuntu-22-04
bind=SUPER,Q,killactive,
bind=SUPER_CTRL,Q,exit,
bind=SUPER_SHIFT,F,togglefloating,
bind=SUPER,SPACE,exec,tofi-drun --drun-launch=true
bind=SUPER,P,exec,kitty pulsemixer
bind=SUPER,F,fullscreen,0
bind=SUPER_CTRL,F,fullscreen,1
bind=SUPER,S,layoutmsg,swapwithmaster
bind=SUPER,M,focusmonitor,+1
bind=SUPER_SHIFT,M,movecurrentworkspacetomonitor,+1

bind=SUPER,left,exec,hypr-tools tmux-focus left
bind=SUPER,right,exec,hypr-tools tmux-focus right
bind=SUPER,up,exec,hypr-tools tmux-focus up
bind=SUPER,down,exec,hypr-tools tmux-focus down
bind=SUPER_SHIFT,left,movewindow,l
bind=SUPER_SHIFT,right,movewindow,r
bind=SUPER_SHIFT,up,movewindow,u
bind=SUPER_SHIFT,down,movewindow,d
bind=SUPER,J,exec,hypr-tools tmux-focus left
bind=SUPER,L,exec,hypr-tools tmux-focus right
bind=SUPER,I,exec,hypr-tools tmux-focus up
bind=SUPER,K,exec,hypr-tools tmux-focus down
bind=SUPER_SHIFT,J,movewindow,l
bind=SUPER_SHIFT,L,movewindow,r
bind=SUPER_SHIFT,I,movewindow,u
bind=SUPER_SHIFT,K,movewindow,d

bind=SUPER,N,changegroupactive
bind=SUPER_SHIFT,N,changegroupactive,b
bind=SUPER,G,togglegroup
bind=SUPER_SHIFT,G,lockactivegroup,toggle
bind=SUPER_CTRL,J,movewindoworgroup,l
bind=SUPER_CTRL,L,movewindoworgroup,r
bind=SUPER_CTRL,I,movewindoworgroup,u
bind=SUPER_CTRL,K,movewindoworgroup,d
bind=SUPER_CTRL,left,movewindoworgroup,l
bind=SUPER_CTRL,right,movewindoworgroup,r
bind=SUPER_CTRL,up,movewindoworgroup,u
bind=SUPER_CTRL,down,movewindoworgroup,d

bind=SUPER,1,workspace,1
bind=SUPER,2,workspace,2
bind=SUPER,3,workspace,3
bind=SUPER,4,workspace,4
bind=SUPER,5,workspace,5
bind=SUPER,6,workspace,6
bind=SUPER,7,workspace,7
bind=SUPER,8,workspace,8
bind=SUPER,9,workspace,9
bind=SUPER,0,workspace,10
bind=SUPER,tab,workspace,previous
#bind=SUPER,0,workspace,name:test

bind=SUPER_SHIFT,1,movetoworkspacesilent,1
bind=SUPER_SHIFT,2,movetoworkspacesilent,2
bind=SUPER_SHIFT,3,movetoworkspacesilent,3
bind=SUPER_SHIFT,4,movetoworkspacesilent,4
bind=SUPER_SHIFT,5,movetoworkspacesilent,5
bind=SUPER_SHIFT,6,movetoworkspacesilent,6
bind=SUPER_SHIFT,7,movetoworkspacesilent,7
bind=SUPER_SHIFT,8,movetoworkspacesilent,8
bind=SUPER_SHIFT,9,movetoworkspacesilent,9
bind=SUPER_SHIFT,0,movetoworkspacesilent,10

bind=CTRL,SPACE,exec,dunstctl close
bind=CTRL_SHIFT,SPACE,exec,dunstctl history-pop

bind=SUPER_CTRL,R,exec,record-screen

bind=SUPER,mouse_down,workspace,e+1
bind=SUPER,mouse_up,workspace,e-1

# bind=,XF86AudioLowerVolume,exec,amixer -D pulse sset Master 5%- && paplay /usr/share/sounds/freedesktop/stereo/audio-volume-change.oga
# bind=,XF86AudioRaiseVolume,exec,amixer -D pulse sset Master 5%+ && paplay /usr/share/sounds/freedesktop/stereo/audio-volume-change.oga
# bind=,XF86AudioMute,exec,amixer -D pulse sset Master toggle-mute
bind=,XF86AudioLowerVolume,exec,volumectl -u down && paplay /usr/share/sounds/freedesktop/stereo/audio-volume-change.oga
bind=,XF86AudioRaiseVolume,exec,volumectl -u up && paplay /usr/share/sounds/freedesktop/stereo/audio-volume-change.oga
bind=,XF86AudioMute,exec,volumectl toggle-mute
bind=SHIFT,XF86AudioLowerVolume,exec,volumectl -m -u down
bind=SHIFT,XF86AudioRaiseVolume,exec,volumectl -m -u up
bind=SHIFT,XF86AudioMute,exec,volumectl -m toggle-mute
bind=,XF86MonBrightnessUp,exec,lightctl up
bind=,XF86MonBrightnessDown,exec,lightctl down
bind=,XF86AudioPlay,exec,playerctl play-pause
bind=,XF86AudioNext,exec,playerctl next
bind=,XF86AudioPrev,exec,playerctl previous
bind=,XF86AudioPause,exec,playerctl pause

#bind=SUPER_SHIFT,P,exec,inktex new
#bind=SUPER_SHIFT,P,exec,notify-send "hey $(echo -e "SETDESC hey\nCONFIRM\nEOT" | pinentry 2>&1)"
bind=SUPER_SHIFT,P,exec,passmenu | wl-copy
bind=,Print,exec,grim /tmp/screenshot.png
# bind=SUPER_SHIFT,S,exec,grim -g "$(slurp)" /tmp/screenshot.png && wl-copy -t image/png < /tmp/screenshot.png
# bind=SUPERFUCKINGSHIFTLMAO,S,exec,hyprctl keyword animation "fadeOut,0,0,default" &&  grim -g "$(slurp)" /tmp/screenshot.png && wl-copy -t image/png < /tmp/screenshot.png; hyprctl keyword animation "fadeOut,1,8,slow"
bind=SUPER_SHIFT,S,exec, hypr-screenshot
bind=SUPER,B,exec,pdfs
bind=SUPER_SHIFT,B,exec,killall -SIGUSR1 waybar
bind=SUPER_SHIFT,X,exec,hyprctl kill
bind=SUPER_CTRL,L,exec,toggle-light

bind=SUPER,PLUS,layoutmsg,addmaster
bind=SUPER,MINUS,layoutmsg,removemaster
bind=SUPER,COMMA,layoutmsg,orientationprev
bind=SUPER,PERIOD,layoutmsg,orientationnext

windowrule=pin,^(showmethekey-gtk)$
windowrule=move 0 85%,^(showmethekey-gtk)$
windowrule=rounding 0,^(showmethekey-gtk)$
windowrule=nofocus,^(showmethekey-gtk)$

windowrule=pin,^(dragon-drop)$
windowrule=tile,^(Spotify)$
windowrule=float,^(yad)$

# Plotting stuff
windowrule=float,^(gksqt)$
windowrule=float,^(gnuplot_qt)$
windowrule=float,^(python3)$ # matplotlib
windowrule=float,^(octave-gui)$
windowrule=float,title:^Firefox — Sharing Indicator$
windowrule=float,title:^nannou -

# bind = SUPER, grave, hyprexpo:expo, toggle # can be: toggle, off/disable or on/enable

plugin {
    hyprexpo {
        columns = 3
        gap_size = 5
        bg_col = rgb(111111)
        workspace_method = first 1 # [center/first] [workspace] e.g. first 1 or center m+1

        enable_gesture = true # laptop touchpad, 4 fingers
        gesture_distance = 300 # how far is the "max"
        gesture_positive = true # positive = swipe down. Negative = swipe up.
    }
}

