#!/usr/bin/env bash

AVAILABLE="$(df --output=avail -h / | sed -n 2p)"
AVAILABLE="${AVAILABLE:1}" # first char is a space
USED="$(df --output=pcent / | sed -n 2p)"
USED="${USED:1:-1}"

TOOLTIP="${USED}% of / used, $AVAILABLE available"
echo -n '{"text": "", "alt": "", "tooltip": "'"$TOOLTIP"'", "class": "", "percentage": '"$USED"' }'
