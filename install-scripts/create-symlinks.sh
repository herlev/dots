#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "$0")")"
cd "$DIR/.." || { echo "Can't cd into $DIR"; exit 1; }
. install-scripts/dots.sh

h1 "Creating symlinks"

link desktop/* ~/.local/share/applications/

relative_to ~
l bin .bin
l fonts .local/share/fonts
l bash/.bashrc .bashrc
l .Xmodmap
l .xprofile
l tmux/tmux.conf .tmux.conf

relative_to ~/.mozilla/firefox
l firefox/profiles.ini profiles.ini
l firefox/chrome profile.default/chrome

relative_to ~/.config
l i3
l polybar
l fish
l lf
l xfce4/terminal
l systemd
l dunst
l nvim
l picom/picom.conf picom.conf
l mimi
l kitty
l zathura
l coc/ultisnips
l bspwm
l sxhkd
l sxiv
l latexmk
l rofi
l xob
l autorandr
l helix
