#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "$0")")"
cd "$DIR/.." || { echo "Can't cd into $DIR"; exit 1; }
. install-scripts/dots.sh

h1 "This is a h1 header"

h2 "This is a h2 header"

h3 "This is a h3 header"
