#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "$0")")"
cd "$DIR" || { echo "Can't cd into $DIR"; exit 1; }
. dots.sh

paks=(
  bspwm               # WM
  sxhkd               # Keyboard daemon
  polybar             # Status bar
  neovim              # Text editor
  helix               # Text editor
  fish                # Shell
  kitty               # Terminal emulator
  zathura             # PDF viewer
  zathura-pdf-mupdf
  firefox             # Web browser
  autorandr
  xsecurelock

  udiskie             # Automount removable disks
  xfce4-power-manager # Power manager
  picom               # Compositor
  dunst               # Notification manager
  yay                 # Aur package installer
  base-devel          # Needed for yay
  dmenu               # Application launcher
  rofi                # Application launcher
  nitrogen            # Wallpaper setter
  light               # Screen brightness utility
  fzf                 # Fuzzy finder
  skim                # another fuzzy finder
  xfce4-screenshooter # Screenshot utility
  pulsemixer          # Pulseaudio CLI
  playerctl           # Used to control music
  blueberry           # Bluetooth manager
  kitty-terminfo      # Terminfo for kitty
  trash-cli
  pass
  passff-host
  networkmanager-dmenu

  chafa               # Terminal image previewer
  yad

  exa                 # ls alternative
  fd                  # find alternative
  ripgrep             # grep alternative
  bat                 # better cat
  jq                  # sed for JSON data
  sd                  # sed alternative
  dog                 # dig alternative
  gping               # ping alternative
  procs               # ps alternative
  xh                  # HTTP request tool
  gdu                 # disk usage analyzer
  dust                # du alternative
  duf                 # df alternative
  bottom              # htop alternative
  tealdeer            # tldr client

  octave              # FOSS matlab alternative

  # Virtual machine
  libvirt
  bridge-utils
  qemu
  edk2-ovmf
  virt-manager
  ebtables

  # Image and vector graphics editor
  inkscape
  gimp

  texlive-bin
  texlive-most

  rustup
  crystal
  shards
  zig
  nodejs
  python-pip
  julia
  gcc-fortran # Required for octave control
  shellcheck

  ccls                # c/c++ language server
  rust-analyzer       # rust language server

  syncthing

  mps-youtube         # Youtube music player
  mpv                 # Video player
  lxappearance-gtk3   # GTK theme changer
  arandr              # Xrandr frontend
  libqalculate        # CLI calculator
  sxiv                # Image viewer
  bluez
  bluez-utils
  newsboat
  npm

  # X utilities
  xorg-xinput
  xorg-xclipboard
  xorg-xev
  xclip
  xtitle
  xcape
  xdotool
  # pavucontrol
)

aur_paks=(
  autojump
  dtach
  pistol-git
  mpv-mpris
  # oomox
  lf
  mimi-git
  mpv-mpris
  dragon-drag-and-drop
  color-picker
  xob
  pastel              # Generate and manipulate colors
  # clone here instead https://github.com/ranger/ranger/blob/master/examples/rifle_sxiv.sh
  # sxiv-rifle          # Makes sxiv view images from a directory
  i3lock-color        # Screen locker
)

h1 "Installing packages"
h2 "Installing normal packages"
sudo pacman -Syyu
sudo pacman -S --needed ${paks[@]}

h2 "Installing aur packages"

# Make sure paru is installed before proceeding
for aur_pak in "${aur_paks[@]}"; do # they somehow don't want to install if specified on same line
  paru -S --needed --noconfirm "$aur_pak"
done

# cargo and npm paks
# pip install --user plantuml
