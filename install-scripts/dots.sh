#!/bin/env bash

# tput color info
# https://linux.101hacks.com/ps1-examples/prompt-color-using-tput/

link_exists() {
  [[ -L ${2} ]] && [[ $(readlink -f "$1") == $(readlink -f "$2") ]] && return 0
  return 1
}

relative_to() {
  mkdir -p "$1"
  relative_to="$1"
}

echov() {
  verbose=${verbose:-false}
  $verbose && echo "$@"
}

display_center(){
  columns="$(tput cols)"
  printf "%*s%*s\n" $(( (${#1} + columns) / 2)) "$1" $(( (${#1} + columns) / 2 - ${#1} )) ""
}

h1() {
  tput bold; tput setaf 4; tput rev
  # echo "# $*"
  display_center "# $* #"
  # display_center "hello world"
  tput sgr0
}

h2() {
  tput setaf 5; tput bold
  echo "# $*"
  # display_center "$*"
  tput sgr0
}

h3() {
  tput setaf 6
  echo "> $*"
  tput sgr0
}

# Checks if the argument is a directory and not a symlink
is_dir() {
  dir=${1%/}
  [[ -d "$dir" ]] && [[ ! -L "$dir" ]] && return 0
  return 1
}

is_empty() {
  [[ "$(ls -A "$1")" ]] || return 0
  return 1
}

link() {
  src="$1"
  target="$2"
  echov trying "$src" "$target"
  mkdir -p "$(dirname "$target")"
  if is_dir "$target"; then # ln is being buggy if a folder already exists
    if is_empty "$target"; then
      rmdir "$target" || exit 1
    else
      echo "$target is a non-empty directory, skipping"
      return 0
    fi
  fi
  if link_exists "$src" "$target"; then
    echov link exists
    return 0
  fi
  ln -svri "$src" "$target"
}

link_relative() {
  if [[ -z "${relative_to}" ]]; then
    echo relative_to not called, exiting
    exit 1
  fi
  src="$1"
  # link_name=${2:-$(basename $src)}
  # If basename is used something like "link xfce4/terminal"
  # wouldn't work
  link_name=${2:-$src}
  target="$relative_to/$link_name"
  link "$src" "$target"
}

yay-needed() {
  echo "Not implemented"
  # https://github.com/Jguer/yay/issues/885
  # yay -S --noconfirm $(yay -Qi $@ 2>&1 >/dev/null | grep "error: package" | grep "was not found" | cut -d"'" -f2 | tr "\n" " ")
  # yay -S --noconfirm --needed $@
}

change_shell() {
  [[ $SHELL = "/usr/bin/$1" ]] || chsh -s "/usr/bin/$1" "$USER"
}

shopt -s expand_aliases
alias l=link_relative

init_msg() {
  which figlet > /dev/null || sudo pacman -Sy --noconfirm --needed figlet > /dev/null
  tput setaf 8
  echo "Using path $PWD"
  tput setaf 2; tput bold
  figlet "Dotfile installer"
  tput sgr0
  tput setaf 8
  echo "press any key to continue"
  read -r
}
