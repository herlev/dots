#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "$0")")"
cd "$DIR/.." || { echo "Can't cd into $DIR"; exit 1; }
. install-scripts/dots.sh

h1 "Performing additional configuration"
h2 "Changing shell to fish"
change_shell fish

h2 "Linking screenlocker to xflock4 for xfce4-power-manager compatability"
sudo su -c ". install-scripts/dots.sh; link $HOME/.bin/lock /usr/bin/xflock4"

h2 "Renaming firefox default profile to profile.default"
tput setaf 8
ff_default_path=~/.mozilla/firefox/profile.default
[[ ! -d $ff_default_path ]] && mv ~/.mozilla/firefox/*.default/ $ff_default_path
